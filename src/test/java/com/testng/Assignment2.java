package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Assignment2 {

	@Test(groups = "santity")
	public void modifiedcustomer() {
		System.out.println("the customer will get modifiedCustomer");
	}

	@Test(dependsOnMethods = "modifiedcustomer")
	public void crateCustomer() {
		System.out.println("the customer will get Customer");

	}

	@Test(groups = "Smoke")
	public void newcustomer() {
		System.out.println("the customer will get new customer");

	}

	@BeforeMethod
	public void beforecustomer() {
		System.out.println("verfy the new customer");
	}

	@AfterClass
	public void afterCustomer() {
		System.out.println("All transections done");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("start data based connection openbrowser");
	}

	@AfterClass
	public void aftercalss() {
		System.out.println("start data based connection closebrowser");
	}


}

	

